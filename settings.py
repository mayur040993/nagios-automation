
#REGION NAME (REQUIRED) to get data from aws particular REGION, example: REGION_NAME='us-west-1'
REGION_NAME=''

#AWS_PROFILE (NOT REQUIRED if using default aws credentials or role is applied to this server)
AWS_PROFILE=''

#LOG_LEVEL (REQUIRED [debug,info,warning,error])
LOG_LEVEL=''

#LOGFILE (REQUIRED path of the log file to store logs, example: LOGFILE='/var/log/nagios.log')
LOGFILE=''

#ENV( NOT REQUIRED if you want to monitor in every env, based on env tags in aws)
ENV=''

#CONF_DIR( Where you have to store all generated conf file, by default in where this script will be run, example:CONF_DIR=/etc/nagios3/conf.d/)
CONF_DIR=''


try:
    from settings_local import *
except:
    print 'No settings_local.py found. Continuing without.'
