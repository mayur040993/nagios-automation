import boto3
import os
import json
from jinja2 import Template
import logging
import sys
import settings
import subprocess


def create_connection():
    """create connection to aws account with particular region and profile_name"""
    try:
        if profile_name:
            session=boto3.session.Session(profile_name=profile_name,region_name=region_name)
            logging.info("connection will be created to aws on region: {0} and profile name: {1}".format(region_name,profile_name))
        else:
            session=boto3.session.Session(region_name=region_name)
            logging.info("connection will be created to aws on region: {0}".format(region_name))
        client=session.client('ec2')
        return client
    except:
        logging.error("check connection to aws")



def get_reservations(env,client):
        """get all running based"""
        dict_ec2={}
        env_list=[]
        type_list=[]
        if env=='all':
            try:
                reservations=client.describe_instances()
                logging.info('getting reservations for all environment')
                connection=1
            except:
                logging.error('connection issue to aws')
        else:
            try:
                reservations=client.describe_instances(Filters=[{'Name':'tag:Env','Values':[env]}])
                logging.info('getting reservations for {0} environment'.format(env))
                connection=1
            except:
                logging.error('connection issue to aws')

        if connection==1:
            for reservation in reservations['Reservations']:
                instances=reservation['Instances']
                for instance in instances:
                    try:
                        tags=instance['Tags']
                        logging.debug('Tags for instance id {0} is : {1}'.format(instance['InstanceId'],tags))
                        if instance['State']['Name'].lower()=="running":
                            instance_name=''
                            instance_monitoring=''
                            instance_env=''
                            instance_type=''
                            instance_svc='[]'
                            for tag in tags:
                                    if tag['Key'].lower()=='name':
                                        instance_name=tag['Value']
                                        instance_name=instance_name.replace(' ','')
                                        instance_name=instance_name.replace(')','')
                                        instance_name=instance_name.replace('(','')
                                    if tag['Key'].lower()=='env':
                                        instance_env=tag['Value']
                                        instance_env=instance_env.replace(' ','')
                                        instance_env=instance_env.replace('(','')
                                        instance_env=instance_env.replace(')','')
                                        env_list.append(instance_env)
                                    if tag['Key'].lower()=='monitoring':
                                        instance_monitoring=tag['Value']
                                    if tag['Key'].lower()=='type':
                                        instance_type=tag['Value']
                                        instance_type=instance_type.replace(' ','')
                                        instance_type=instance_type.replace('(','')
                                        instance_type=instance_type.replace(')','')
                                        type_list.append(instance_type)
                                    if tag['Key'].lower()=='nagios_services':
                                        instance_svc=tag['Value']
                            if not instance_name:
                                    instance_name=instance['PrivateIpAddress']
                            if not instance_env:
                                    instance_env="no_env"
                                    env_list.append(instance_env)
                            if not instance_monitoring:
                                    instance_monitoring='no_monitoring'
                            if not instance_type:
                                    instance_type='no_type'
                                    type_list.append(instance_type)
                            dict_ec2[instance['PrivateIpAddress']]=[instance_name,instance_env,instance_monitoring,instance_type,instance_svc]
                            logging.debug("Information for {0} Host: {1}".format(instance['PrivateIpAddress'],dict_ec2[instance['PrivateIpAddress']]))
                            instance_name=''
                            instance_monitoring=''
                            instance_env=''
                    except:
                        logging.error("One of Important tag from [Name,Env,Monitoring,Type,Nagios_services] not set on {0}".format(instance['InstanceId']))


            logging.debug("Environment present in region {0} is : {1} ".format(region_name,list(set(env_list))))
            logging.debug("Type of Servers is {0}".format(list(set(type_list))))
            return dict_ec2,env_list,type_list
        else:
            logging.error('connection issue to aws')
            exit()

def check_hostgroup_cfg(env_list,type_list,path):
    """hostgroups created based on env"""

    dir_path=path+'/hostgroups'
    if not os.path.exists(dir_path):
        logging.info('creating hostgroups directory at {0}'.format(path))
        os.makedirs(dir_path)

    path_iter=os.walk(dir_path)
    root,dirs,files=path_iter.next()
    i=0
    list_file=[]

    for data in env_list:
        hostgroup=type_list[i]+'-'+data
        if hostgroup+".cfg" in files:
            logging.info("Already created {0}.cfg File in hostgroups directory".format(hostgroup))
        else:
            logging.info("Creating {0}.cfg hostgroup file in hostgroups directory".format(hostgroup))
            var={'hostgroup':hostgroup}
            infile='templates/hostgroup.j2'
            outfile=dir_path+'/'+hostgroup+".cfg"
            generate_cfg(var=var,infile=infile,outfile=outfile)

        list_file.append(type_list[i]+'-'+data+".cfg")
        i=i+1

    file_diff=set(files).difference(set(list_file))
    for fi in list(file_diff):
        logging.info('removing unused {0} file from hostgroups folder'.format(fi))
        os.remove(dir_path+'/'+fi)

def check_host_cfg(dict_ec2,path):
    """check nagios file is present in particular path or not"""

    dir_path=path+'/hosts'
    if not os.path.exists(dir_path):
        logging.info('creating hosts directory at {0}'.format(path))
        os.makedirs(dir_path)

    path_iter=os.walk(dir_path)
    root,dirs,files=path_iter.next()
    list_host=[]
    for data in dict_ec2.keys():

        if data+".cfg" in files:
            if dict_ec2[data][2]=='no_monitoring':
                logging.info('removing {0} file from hosts folder as does not contain monitoring tag'.format(data+".cfg"))
                os.remove(dir_path+'/'+data+".cfg")
            logging.info("Already created {0}.cfg file in hosts directory".format(data+".cfg"))
            files.remove(data+".cfg")
        else:
            if dict_ec2[data][1]!='no_env' and dict_ec2[data][2]!='no_monitoring' and dict_ec2[data][3]!='no_type':
                logging.info("Creating {0}.cfg host file in hosts directory".format(data))
                ip=data
                hostgroup=dict_ec2[data][3]+'-'+dict_ec2[data][1]
                hostname=dict_ec2[data][0]+'-'+data
                var={'hostgroup':hostgroup,'hostname':hostname,'ip':data}
                infile='templates/host.j2'
                outfile=dir_path+'/'+ip+".cfg"
                generate_cfg(var=var,infile=infile,outfile=outfile)
            else:
                logging.warning("Env is not set or monitoring tag doesnot contain nagios or type tag is not set in {0} {1}".format(dict_ec2[data][0],data))

    #removing unused files
    for fi in files:
        logging.info('removing unused {0} file from hosts folder'.format(fi))
        os.remove(dir_path+'/'+fi)


def check_services_cfg(dict_ec2,path):
    "Creating services for each hostgroups"

    dir_path=path+'/services'
    if not os.path.exists(dir_path):
        logging.info('creating services directory at {0}'.format(path))
        os.makedirs(dir_path)
    path_iter=os.walk(dir_path)
    root,dirs,files=path_iter.next()
    list_general=[]
    for data in dict_ec2.keys():
        svc_dir=dir_path+'/'+dict_ec2[data][3]+'-'+dict_ec2[data][1]
        hostgroup=dict_ec2[data][3]+'-'+dict_ec2[data][1]
        if not os.path.exists(svc_dir):
            logging.info('creating hostgroup directory {0} inside services which contains its related services'.format(hostgroup))
            os.makedirs(svc_dir)
        path_iter_svc=os.walk(svc_dir)
        root_svc,dirs_svc,files_svc=path_iter_svc.next()
        for sf in json.loads(dict_ec2[data][4]):
            sf_parse=sf.split('!')
            if sf_parse[0]!='general':
                if sf_parse[0]+".cfg" in files_svc:
                    logging.debug("Already created {0}.cfg at {1} hostgroup".format(sf_parse[0],hostgroup))
                    files_svc.remove(sf_parse[0]+".cfg")
                else:
                    if dict_ec2[data][1]!='no_env' and dict_ec2[data][2]!='no_monitoring' and dict_ec2[data][3]!='no_type':
                        logging.debug("Creating {0}.cfg at {1} hostgroup".format(sf_parse[0],hostgroup))
                        outfile=svc_dir+'/'+sf_parse[0]+".cfg"
                        if(sf_parse[0].find('nrpe')>=0):
                            sf_list=sf_parse[0].split('-')
                            var={'hostgroup':hostgroup,'service_name':sf_list[0].upper(),'service_cmd':sf_list[0]}
                            infile='templates/service_nrpe.j2'
                        else:
                            var={'hostgroup':hostgroup,'service_name':sf_parse[0].upper(),'service_cmd':sf}
                            infile='templates/service.j2'
                        generate_cfg(var=var,infile=infile,outfile=outfile)
                    else:
                        logging.warning("Env is not set or monitoring tag doesnot contain nagios or type tag is not set in {0} {1}".format(dict_ec2[data][0],data))
            else:
                list_general.append(dict_ec2[data][3]+'-'+dict_ec2[data][1])
        #removing unused files
        for fi in files_svc:
            logging.info('removing unused {0} file from {1} folder'.format(fi,svc_dir))
            os.remove(svc_dir+'/'+fi)
    #creating general services for all related hostgroups
    creating_general_cfg(list_general,path)



def creating_general_cfg(list_general,path):
    if len(list_general)!=0:
        if not os.path.exists(path+'/general'):
            logging.info('creating general directory at {0}'.format(path))
            os.makedirs(path+'/general')
        var={'hostgroups':','.join(list_general)}
        generate_cfg(var=var, infile='templates/general.j2',outfile=path+'/general/general.cfg')
    else:
        if os.path.isfile(path+'/general/general.cfg'):
            os.remove(path+'/general/general.cfg')

def generate_cfg(var,infile,outfile):
    logging.info('creating {0} from {1}'.format(outfile,infile))
    filein = open(infile)
    fileout = open(outfile,'w+')
    src = Template( filein.read() )
    result = src.render(var)
    dst = fileout.write(result)





def main():
    LEVELS = {'debug': logging.DEBUG,
              'info': logging.INFO,
              'warning': logging.WARNING,
              'error': logging.ERROR,
              'critical': logging.CRITICAL}

    level = LEVELS.get(log_level, logging.NOTSET)
    FORMAT = '[%(asctime)s] %(levelname)s [%(name)s:%(funcName)s:%(lineno)s] --- %(message)s'
    log=logging.basicConfig(format=FORMAT,level=level,filename=logfile)
    logging.getLogger(__name__)

    client=create_connection()
    dict_ec2,env_list,type_list=get_reservations(env,client)
    check_host_cfg(dict_ec2,conf_dir)
    check_hostgroup_cfg(env_list,type_list,conf_dir)
    check_services_cfg(dict_ec2,conf_dir)




if __name__=="__main__":
    region_name=settings.REGION_NAME
    profile_name=settings.AWS_PROFILE
    logfile=settings.LOGFILE
    log_level=settings.LOG_LEVEL
    if settings.ENV:
        env=settings.ENV
    else:
        env='all'
    if settings.CONF_DIR:
        conf_dir=settings.CONF_DIR
    else:
        conf_dir=os.getcwd()
    main()
